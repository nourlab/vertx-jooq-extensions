package io.cloudonix.lib.model.support;

import java.io.File;
import java.util.Arrays;

import org.jooq.codegen.GeneratorStrategy;
import org.jooq.meta.Definition;
import org.jooq.meta.TypedElementDefinition;
import org.jooq.tools.JooqLogger;

import io.github.jklingsporn.vertx.jooq.generate.VertxGeneratorStrategy;

public class ExtendedVertxGeneratorStrategy extends VertxGeneratorStrategy {

	enum JsonKeyNameStyle {
		ORIGINAL,
		CAMEL,
		CAMEL_PROPER,
		KEBAB,
		SNAKE,
	}
	
	private static final JooqLogger log = JooqLogger.getLogger(ExtendedVertxGeneratorStrategy.class);
	
	private JsonKeyNameStyle jsonKeyNameStyle;
	
	public ExtendedVertxGeneratorStrategy() {
		super();
		String propVal = System.getProperty("io.cloudonix.lib.model.json-key-style", "").replaceAll("[^a-z]+", "");
		jsonKeyNameStyle = Arrays.stream(JsonKeyNameStyle.values())
				.map(e -> e.name())
				.filter(name -> name.replaceAll("(?:i)[^a-z]+", "").equalsIgnoreCase(propVal))
				.findFirst().map(JsonKeyNameStyle::valueOf).orElse(JsonKeyNameStyle.ORIGINAL);
	}
	
	public ExtendedVertxGeneratorStrategy(GeneratorStrategy st) {
		super(st);
	}

	@Override
	public String getJsonKeyName(TypedElementDefinition<?> column) {
		switch (jsonKeyNameStyle) {
		case ORIGINAL:
			return super.getJsonKeyName(column);
		case CAMEL:
			return getJavaMemberName(column, Mode.POJO);
		case CAMEL_PROPER:
			String name = getJavaMemberName(column, Mode.POJO);
			return name.substring(0,1).toUpperCase() + name.substring(1);
		case KEBAB:
		default:
			return super.getJsonKeyName(column).replaceAll("_", "-");
		case SNAKE:
			return super.getJsonKeyName(column).replaceAll("_", "-");
		}
	}
	
	@Override
	public final String getFullJavaClassName(Definition definition, Mode mode) {
		if (hasConcreteClass(this, definition, mode)) {
			log.info("Generating alternative POJO name for " + definition);
			return getImplPkg(getJavaPackageName(definition, mode)) +
					"." + getJavaClassName(definition, mode);
		}
		return super.getFullJavaClassName(definition, mode);
	}

	public static String getImplPkg(String originalPojoPkg) {
		return System.getProperty("io.cloudonix.lib.model.concrete-package", originalPojoPkg + ".concrete");
	}

	public static String getSourceDirectory() {
		return System.getProperty("io.cloudonix.lib.model.source-directory", "src/main/java");
	}
	
	public static boolean hasConcreteClass(GeneratorStrategy st, Definition definition, Mode mode) {
		String concretePkg = getImplPkg(st.getJavaPackageName(definition, mode));
		File implFile = new File(getSourceDirectory() + File.separatorChar +
				concretePkg.replace('.', File.separatorChar) + File.separatorChar +
				st.getJavaClassName(definition, mode) + ".java");
		return implFile.exists();
	}

}
