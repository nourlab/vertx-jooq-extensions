package io.cloudonix.lib.model.support;

import org.jooq.meta.TypedElementDefinition;

import io.github.jklingsporn.vertx.jooq.generate.VertxGeneratorStrategy;

public class JsonJooqStrategy extends VertxGeneratorStrategy {
	
	@Override
	public String getJsonKeyName(TypedElementDefinition<?> column) {
		return super.getJsonKeyName(column).replaceAll("_", "-");
	}

}
