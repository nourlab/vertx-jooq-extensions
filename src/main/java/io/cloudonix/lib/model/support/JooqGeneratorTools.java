package io.cloudonix.lib.model.support;

import org.jooq.codegen.JavaWriter;
import org.jooq.meta.TableDefinition;

import com.google.common.base.CaseFormat;

public class JooqGeneratorTools {
	
	public static void generateMerge(TableDefinition table, JavaWriter out) {
		out.println();
		out.tab(1).println("public " + CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, table.getName()) + " merge(io.vertx.core.json.JsonObject json) {");
		out.tab(2).println("this.fromJson(toJson().mergeIn(json, false));");
		out.tab(2).println("return this;");
		out.tab(1).println("}");
	}

}
