package io.cloudonix.lib.model.support;

import java.io.File;

import org.jooq.codegen.JavaWriter;
import org.jooq.meta.TableDefinition;

import io.github.jklingsporn.vertx.jooq.generate.completablefuture.CompletableFutureJDBCGuiceVertxGenerator;

public class ExtendedCompletableFutureJDBCGuiceVertxGenerator extends CompletableFutureJDBCGuiceVertxGenerator {

	@Override
	protected void generateDao(TableDefinition table, JavaWriter out) {
		((OverridableJavaWriter) out).setTable(table, getStrategy());
		super.generateDao(table, out);
	}

	@Override
	protected JavaWriter newJavaWriter(File file) {
		return new OverridableJavaWriter(file, generateFullyQualifiedTypes(), targetEncoding);
	}

	@Override
	protected void generatePojoClassFooter(TableDefinition table, JavaWriter out) {
		super.generatePojoClassFooter(table, out);
		JooqGeneratorTools.generateMerge(table, out);
	}

}
