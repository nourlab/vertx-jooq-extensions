package io.cloudonix.lib.model;

import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.ibatis.jdbc.ScriptRunner;

/**
 * Manage schema deployment and upgrades for APIModel
 * @author odeda
 *
 */
public class SchemaManager {
	
	public Iterable<String> getSchemas() {
		return getSchemas(1);
	}

	public Iterable<String> getSchemas(int startFrom) {
		List<String> pads = IntStream.range(0, 3)
				.mapToObj(i -> String.join("", Collections.nCopies(i, "0").toArray(new String[] {})))
				.collect(Collectors.toList());
		return new Iterable<String>() {
			
			@Override
			public Iterator<String> iterator() {
				ClassLoader cl = Thread.currentThread().getContextClassLoader();
				return new Iterator<String>() {
					int index = startFrom;
					Optional<String> nextVal = findNextVal(index);
					
					@Override
					public String next() {
						try {
							return nextVal.get();
						} finally {
							nextVal = findNextVal(++index);
						}
					}
					
					private Optional<String> findNextVal(int i) {
						return pads.stream().map(p -> "schema/schema-" + p + i + ".sql")
								.filter(n -> Objects.nonNull(cl.getResource(n)))
								.findAny();
					}
					
					@Override
					public boolean hasNext() {
						return nextVal.isPresent();
					}
				};
			}
		};
	}
	
	public void runScript(Connection con, String sql) {
		runScript(con, new StringReader(sql));
	}
	
	public void runScript(Connection con, InputStream sql) {
		runScript(con, new InputStreamReader(sql, StandardCharsets.UTF_8));
	}
	
	public void runScript(Connection con, Reader sql) {
		ScriptRunner sr = new ScriptRunner(con);
		sr.setAutoCommit(true);
		sr.setLogWriter(new PrintWriter(new StringWriter()));
		sr.runScript(sql);
	}
	
	public void updateSchema(Connection con, int currentVersion) throws SQLException {
		for (String schema : getSchemas(currentVersion + 1)) {
			URL schemaUrl = Thread.currentThread().getContextClassLoader().getResource(schema);
			if (Objects.isNull(schemaUrl))
				throw new SQLException("Failed to locate schema " + schema);
			try {
				runScript(con, schemaUrl.openStream());
			} catch (IOException e) {
				throw new SQLException("Failed to open schema: " + e,e);
			}
		}
	}
	

}
