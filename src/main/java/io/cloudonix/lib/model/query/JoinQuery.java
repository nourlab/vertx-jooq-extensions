package io.cloudonix.lib.model.query;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;

import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.ForeignKey;
import org.jooq.JoinType;
import org.jooq.OrderField;
import org.jooq.Record;
import org.jooq.ResultQuery;
import org.jooq.SelectJoinStep;
import org.jooq.SelectSelectStep;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UpdatableRecord;
import org.jooq.exception.DataAccessException;

import io.github.jklingsporn.vertx.jooq.completablefuture.jdbc.JDBCCompletableFutureQueryExecutor;
import io.github.jklingsporn.vertx.jooq.shared.internal.AbstractVertxDAO;

public class JoinQuery<R1 extends UpdatableRecord<R1>, P1, R2 extends UpdatableRecord<R2>, P2> {

	JoinQuery<?, ?, R1, P1> parent;
	QueryManager<R1, P1> left;
	QueryManager<R2, P2> right;
	private JoinType type;
	private Condition condition;

	public JoinQuery(QueryManager<R1, P1> left, QueryManager<R2, P2> right, JoinType type, Condition on) {
		this.left = left;
		this.right = right;
		this.type = type;
		this.condition = on;
	}

	private JoinQuery(JoinQuery<?, ?, R1, P1> left, QueryManager<R2, P2> right, JoinType type, Condition on) {
		this(left.right, right, type, on);
		this.parent = left;
	}
	
	public <R3 extends UpdatableRecord<R3>, P3> JoinQuery<R2, P2, R3, P3> with(QueryManager<R3, P3> right) {
		return with(right, JoinType.JOIN, null);
	}

	public <R3 extends UpdatableRecord<R3>, P3> JoinQuery<R2, P2, R3, P3> with(AbstractVertxDAO<R3,P3, Integer, CompletableFuture<List<P3>>, CompletableFuture<P3>, CompletableFuture<Integer>, CompletableFuture<Integer>> right) {
		return with(new QueryManager<R3,P3>(right), JoinType.JOIN, null);
	}

	public <R3 extends UpdatableRecord<R3>, P3> JoinQuery<R2, P2, R3, P3> with(QueryManager<R3, P3> right, Condition on) {
		return with(right, JoinType.JOIN, on);
	}

	public <R3 extends UpdatableRecord<R3>, P3> JoinQuery<R2, P2, R3, P3> with(AbstractVertxDAO<R3,P3, Integer, CompletableFuture<List<P3>>, CompletableFuture<P3>, CompletableFuture<Integer>, CompletableFuture<Integer>> right,
			Condition on) {
		return with(new QueryManager<R3,P3>(right), JoinType.JOIN, on);
	}

	public <R3 extends UpdatableRecord<R3>, P3> JoinQuery<R2, P2, R3, P3> with(QueryManager<R3, P3> right,
			JoinType type) {
		return with(right, type, null);
	}

	public <R3 extends UpdatableRecord<R3>, P3> JoinQuery<R2, P2, R3, P3> with(AbstractVertxDAO<R3,P3, Integer, CompletableFuture<List<P3>>, CompletableFuture<P3>, CompletableFuture<Integer>, CompletableFuture<Integer>> right,
			JoinType type) {
		return with(new QueryManager<R3,P3>(right), type, null);
	}

	public <R3 extends UpdatableRecord<R3>, P3> JoinQuery<R2, P2, R3, P3> with(QueryManager<R3, P3> right,
			JoinType type, Condition on) {
		return new JoinQuery<>(this, right, type, on);
	}

	public <R3 extends UpdatableRecord<R3>, P3> JoinQuery<R2, P2, R3, P3> with(AbstractVertxDAO<R3,P3, Integer, CompletableFuture<List<P3>>, CompletableFuture<P3>, CompletableFuture<Integer>, CompletableFuture<Integer>> right,
			JoinType type, Condition on) {
		return new JoinQuery<>(this, new QueryManager<R3,P3>(right), type, on);
	}

	public <R3 extends UpdatableRecord<R3>, P3> JoinQuery<R2, P2, R3, P3> leftJoin(QueryManager<R3, P3> right) {
		return with(right, JoinType.LEFT_OUTER_JOIN, null);
	}

	public <R3 extends UpdatableRecord<R3>, P3> JoinQuery<R2, P2, R3, P3> leftJoin(QueryManager<R3, P3> right,
			Condition on) {
		return with(right, JoinType.LEFT_OUTER_JOIN, on);
	}
	
	public <R3 extends UpdatableRecord<R3>, P3> JoinQuery<R2, P2, R3, P3> leftJoin(AbstractVertxDAO<R3,P3, Integer, CompletableFuture<List<P3>>, CompletableFuture<P3>, CompletableFuture<Integer>, CompletableFuture<Integer>> right) {
		return with(right, JoinType.LEFT_OUTER_JOIN, null);
	}

	public <R3 extends UpdatableRecord<R3>, P3> JoinQuery<R2, P2, R3, P3> leftJoin(AbstractVertxDAO<R3,P3, Integer, CompletableFuture<List<P3>>, CompletableFuture<P3>, CompletableFuture<Integer>, CompletableFuture<Integer>> right,
			Condition on) {
		return with(right, JoinType.LEFT_OUTER_JOIN, on);
	}

	public <R3 extends UpdatableRecord<R3>, P3> JoinQuery<R2, P2, R3, P3> rightJoin(QueryManager<R3, P3> right) {
		return with(right, JoinType.RIGHT_OUTER_JOIN, null);
	}

	public <R3 extends UpdatableRecord<R3>, P3> JoinQuery<R2, P2, R3, P3> rightJoin(QueryManager<R3, P3> right,
			Condition on) {
		return with(right, JoinType.RIGHT_OUTER_JOIN, on);
	}

	public <R3 extends UpdatableRecord<R3>, P3> JoinQuery<R2, P2, R3, P3> rightJoin(AbstractVertxDAO<R3,P3, Integer, CompletableFuture<List<P3>>, CompletableFuture<P3>, CompletableFuture<Integer>, CompletableFuture<Integer>> right) {
		return with(new QueryManager<R3,P3>(right), JoinType.RIGHT_OUTER_JOIN, null);
	}

	public <R3 extends UpdatableRecord<R3>, P3> JoinQuery<R2, P2, R3, P3> rightJoin(AbstractVertxDAO<R3,P3, Integer, CompletableFuture<List<P3>>, CompletableFuture<P3>, CompletableFuture<Integer>, CompletableFuture<Integer>> right,
			Condition on) {
		return with(new QueryManager<R3,P3>(right), JoinType.RIGHT_OUTER_JOIN, on);
	}

	private Table<R1> leftTable() {
		return left.dao.getTable();
	}
	
	private Table<R2> rightTable() {
		return right.dao.getTable();
	}
	
	private <RRight extends Record> Condition computeForeignKey(Table<RRight> rightTable) {
		if (Objects.nonNull(condition))
			return condition;
		Condition cond = computerForeignKey(leftTable(), rightTable);
		if (Objects.nonNull(cond))
			return cond;
		if (Objects.isNull(parent))
			throw new DataAccessException("No relation found to " + rightTable);
		return parent.computeForeignKey(rightTable);
	}
	
	@SuppressWarnings("unchecked")
	private <RRight extends Record> Condition computerForeignKey(Table<R1> left, Table<RRight> right) {
		Optional<ForeignKey<R1, ?>> candidateL2R = left.getReferences().stream()
				.filter(fk -> fk.getKey().getTable().getName().equals(right.getName())).findFirst();
		if (candidateL2R.isPresent()) {
			ForeignKey<R1, RRight> fk = (ForeignKey<R1, RRight>) candidateL2R.get();
			TableField<R1, Integer> myField = (TableField<R1, Integer>) fk.getFieldsArray()[0];
			return myField.eq((TableField<RRight, Integer>) fk.getKey().getFieldsArray()[0]);
		}
		Optional<ForeignKey<RRight, ?>> candidateR2L = right.getReferences().stream()
				.filter(fk -> fk.getKey().getTable().getName().equals(left.getName())).findFirst();
		if (candidateR2L.isPresent()) {
			ForeignKey<RRight, R1> fk = (ForeignKey<RRight, R1>) candidateR2L.get();
			TableField<RRight, Integer> myField = (TableField<RRight, Integer>) fk.getFieldsArray()[0];
			return myField.eq((TableField<R1, Integer>) fk.getKey().getFieldsArray()[0]);
		}
		return null;
	}
	
	private Function<DSLContext, SelectSelectStep<Record>> selectLeftFieldsQuery() {
		return dsl -> dsl.select(leftTable().fields()); 
	}
	
	private Function<DSLContext, SelectSelectStep<Record>> selectFieldsQuery() { 
		return (Objects.nonNull(parent) ? parent.selectFieldsQuery() : selectLeftFieldsQuery())
				.andThen(sel -> sel.select(rightTable().fields()));
	}
	
	private Function<SelectSelectStep<Record>, SelectJoinStep<Record>> fromLeftTable = sel -> sel.from(leftTable());
	
	private Function<SelectSelectStep<Record>, SelectJoinStep<Record>> joinTableQuery() {
		return (Objects.nonNull(parent) ? parent.joinTableQuery() : fromLeftTable)
				.andThen(sel -> sel.join(rightTable(), type).on(computeForeignKey(rightTable())));
	}
	
	public CompletableFuture<ResultsCollector> findManyByCondition(Condition condition) {
		return query(sel -> sel.where(condition));
	}

	public CompletableFuture<ResultsCollector> findManyByCondition(Condition condition, OrderField<?>... orderField) {
		return query(sel -> sel.where(condition).orderBy(orderField));
	}
	
	public CompletableFuture<ResultsCollector> findOneByCondition(Condition condition) {
		return query(sel -> sel.where(condition).limit(1));
	}
	
	public CompletableFuture<ResultsCollector> query(Function<SelectJoinStep<Record>, ResultQuery<Record>> query) {
		return leftMostExecutor().query(selectFieldsQuery().andThen(joinTableQuery()).andThen(query))
				.thenApply(qr -> new ResultsCollector(qr, collectQueueManagers()));
	}
	
	private List<QueryManager<?,?>> collectQueueManagers() {
		ArrayList<QueryManager<?,?>> leftList = new ArrayList<>(Objects.nonNull(parent) ? parent.collectQueueManagers() : Collections.singletonList(left));
		leftList.add(right);
		return leftList;
	}

	private JDBCCompletableFutureQueryExecutor<?, ?, ?> leftMostExecutor() {
		if (Objects.nonNull(parent))
			return parent.leftMostExecutor();
		return (JDBCCompletableFutureQueryExecutor<R1, P1, Integer>) left.dao.queryExecutor();
	}

}