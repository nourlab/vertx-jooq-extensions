package io.cloudonix.lib.model.query;

import java.util.*;
import java.util.concurrent.CompletableFuture;

import org.jooq.*;
import org.jooq.impl.DSL;

import com.google.inject.Inject;

import io.github.jklingsporn.vertx.jooq.shared.internal.AbstractVertxDAO;

public class QueryManager<R extends UpdatableRecord<R>, P> {

	AbstractVertxDAO<R, P, Integer, CompletableFuture<List<P>>, CompletableFuture<P>, CompletableFuture<Integer>, CompletableFuture<Integer>> dao;

	@Inject
	public QueryManager(
			AbstractVertxDAO<R, P, Integer, CompletableFuture<List<P>>, CompletableFuture<P>, CompletableFuture<Integer>, CompletableFuture<Integer>> dao) {
		this.dao = dao;
	}
	
	public <R2 extends UpdatableRecord<R2>,P2> JoinQuery<R,P,R2,P2> with(QueryManager<R2,P2> right) {
		return new JoinQuery<>(this, right, JoinType.JOIN, null);
	}

	public <R2 extends UpdatableRecord<R2>,P2> JoinQuery<R,P,R2,P2> with(AbstractVertxDAO<R2,P2, Integer, CompletableFuture<List<P2>>, CompletableFuture<P2>, CompletableFuture<Integer>, CompletableFuture<Integer>> right) {
		return new JoinQuery<>(this, new QueryManager<R2,P2>(right), JoinType.JOIN, null);
	}
	
	public <R2 extends UpdatableRecord<R2>,P2> JoinQuery<R,P,R2,P2> with(QueryManager<R2,P2> right, Condition on) {
		return new JoinQuery<>(this, right, JoinType.JOIN, on);
	}

	public <R2 extends UpdatableRecord<R2>,P2> JoinQuery<R,P,R2,P2> with(AbstractVertxDAO<R2,P2, Integer, CompletableFuture<List<P2>>, CompletableFuture<P2>, CompletableFuture<Integer>, CompletableFuture<Integer>> right, Condition on) {
		return new JoinQuery<>(this, new QueryManager<R2,P2>(right), JoinType.JOIN, on);
	}
	
	public <R2 extends UpdatableRecord<R2>,P2> JoinQuery<R,P,R2,P2> leftJoin(QueryManager<R2,P2> right) {
		return new JoinQuery<>(this, right, JoinType.LEFT_OUTER_JOIN, null);
	}

	public <R2 extends UpdatableRecord<R2>,P2> JoinQuery<R,P,R2,P2> leftJoin(AbstractVertxDAO<R2,P2, Integer, CompletableFuture<List<P2>>, CompletableFuture<P2>, CompletableFuture<Integer>, CompletableFuture<Integer>> right) {
		return new JoinQuery<>(this, new QueryManager<R2,P2>(right), JoinType.LEFT_OUTER_JOIN, null);
	}
	
	public <R2 extends UpdatableRecord<R2>,P2> JoinQuery<R,P,R2,P2> leftJoin(QueryManager<R2, P2> right, Condition on) {
		return new JoinQuery<>(this, right, JoinType.LEFT_OUTER_JOIN, on);
	}

	public <R2 extends UpdatableRecord<R2>,P2> JoinQuery<R,P,R2,P2> leftJoin(AbstractVertxDAO<R2,P2, Integer, CompletableFuture<List<P2>>, CompletableFuture<P2>, CompletableFuture<Integer>, CompletableFuture<Integer>> right, Condition on) {
		return new JoinQuery<>(this, new QueryManager<R2,P2>(right), JoinType.LEFT_OUTER_JOIN, on);
	}
	
	public <R2 extends UpdatableRecord<R2>,P2> JoinQuery<R,P,R2,P2> rightJoin(QueryManager<R2,P2> right) {
		return new JoinQuery<>(this, right, JoinType.RIGHT_OUTER_JOIN, null);
	}

	public <R2 extends UpdatableRecord<R2>,P2> JoinQuery<R,P,R2,P2> rightJoin(AbstractVertxDAO<R2,P2, Integer, CompletableFuture<List<P2>>, CompletableFuture<P2>, CompletableFuture<Integer>, CompletableFuture<Integer>> right) {
		return new JoinQuery<>(this, new QueryManager<R2,P2>(right), JoinType.RIGHT_OUTER_JOIN, null);
	}
	
	public <R2 extends UpdatableRecord<R2>,P2> JoinQuery<R,P,R2,P2> rightJoin(QueryManager<R2,P2> right, Condition on) {
		return new JoinQuery<>(this, right, JoinType.RIGHT_OUTER_JOIN, on);
	}

	public <R2 extends UpdatableRecord<R2>,P2> JoinQuery<R,P,R2,P2> rightJoin(AbstractVertxDAO<R2,P2, Integer, CompletableFuture<List<P2>>, CompletableFuture<P2>, CompletableFuture<Integer>, CompletableFuture<Integer>> right, Condition on) {
		return new JoinQuery<>(this, new QueryManager<R2,P2>(right), JoinType.RIGHT_OUTER_JOIN, on);
	}
	
	public static Condition reduceConditions(List<Condition> conditions) {
		if (conditions.isEmpty())
			return DSL.trueCondition();
		Condition out = conditions.remove(0);
		while (!conditions.isEmpty())
			out = out.and(conditions.remove(0));
		return out;
	}

	public Table<R> getTable() {
		return dao.getTable();
	}

	public Class<P> getType() {
		return dao.getType();
	}
	
	public AbstractVertxDAO<R,P,Integer,CompletableFuture<List<P>>,CompletableFuture<P>,CompletableFuture<Integer>,CompletableFuture<Integer>> getDao() {
		return dao;
	}

}
